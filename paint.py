from logging import getLogger
logger = getLogger("logger")


class Paint(object):
    """
    Paint class.
    """
    def __init__(self):
        """
        initialization: create canvas object
        """
        def canvas_to_str(self):
            if self.canvas is None:
                
                return str()
            else:
                from os import linesep
                border = "-" * (self.w + 2)
                output = (
                    str()
                    .join(map(lambda row: "|" + str().join(row) + "|" + linesep, self.canvas))
                    .join([border + linesep, border])
                )

            return output

        self._canvas = type(
            "Canvas",
            (object,),
            dict(
                canvas=None,
                w=0, h=0,
                __str__=canvas_to_str,
                __bool__=lambda self: bool(self.canvas),
                __getitem__=lambda self, i: self.canvas[i],
                __contains__=lambda self, p: 0 <= p[1] < self.h and 0 <= p[0] < self.w,
            ),
        )()

    @property
    def canvas(self):
        return str(self._canvas)

    def create_canvas(self, w, h):
        """
        create canvas by size parameters
        """
        try:
            w, h = int(w), int(h)
            assert (w > 0 and h > 0), "-nonpositive canvas size parameters."
            assert (w <= 2048 and h <= 2048), "-canvas oversize."

            self._canvas.w, self._canvas.h = w, h
            self._canvas.canvas = [[" "] * w for i in range(h)]
            
        except AssertionError as e:
            logger.warning(e)
        except ValueError:
            logger.warning("-invalid canvas size parameters.")
        except Exception as e:
            logger.debug(e)
            logger.error("-canvas can not be created.")
        else:
            logger.info(self.canvas)

    def _draw_line(self, index, diapason, direction, c):
        """
        draws horizontal or vertical line on canvas
        """
        if direction == "H":
            self._canvas[index][diapason[0]:diapason[1] + 1] = [c] * (diapason[1] - diapason[0] + 1)
        elif direction == "V":
            for i in range(diapason[0], diapason[1] + 1):
                self._canvas[i][index] = c
        else:
            raise Exception("-invalid line direction.")

    def create_line(self, x1, y1, x2, y2, c="x"):
        """
        create line by points coordinates
        currently only horizontal or vertical lines are supported
        """
        try:
            if not self._canvas:
                raise Warning("-canvas was not created.")

            assert len(c) == 1 and isinstance(c, str), '-"color" parameter is not a symbol.'

            x1, y1, x2, y2 = int(x1) - 1, int(y1) - 1, int(x2) - 1, int(y2) - 1
            if x1 == x2 and (x1, y1) in self._canvas and (x2, y2) in self._canvas:
                self._draw_line(x1, sorted([y1, y2]), "V", c)
            elif y1 == y2 and (x1, y1) in self._canvas and (x2, y2) in self._canvas:
                self._draw_line(y1, sorted([x1, x2]), "H", c)
            else:
                raise AssertionError("-invalid points coordinates.")

        except AssertionError as e:
            logger.warning(e)
        except Warning as e:
            logger.warning(e)
        except ValueError:
            logger.warning("-invalid points coordinates parameters.")
        except Exception as e:
            logger.debug(e)
            logger.error("-line can not be created.")
        else:
            logger.info(self.canvas)

    def create_rectangle(self, x1, y1, x2, y2, c="x"):
        """
        create rectangle by top left and bottom right corners points coordinates
        """
        try:
            if not self._canvas:
                raise Warning("-canvas was not created.")

            assert len(c) == 1 and isinstance(c, str), '-"color" parameter is not a symbol.'

            x1, y1, x2, y2 = int(x1) - 1, int(y1) - 1, int(x2) - 1, int(y2) - 1
            if (x1, y1) in self._canvas and (x2, y2) in self._canvas:
                y = sorted([y1, y2])
                x = sorted([x1, x2])
                self._draw_line(x[0], y, "V", c)
                self._draw_line(x[1], y, "V", c)
                self._draw_line(y[0], x, "H", c)
                self._draw_line(y[1], x, "H", c)
            else:
                raise AssertionError("-invalid points coordinates.")

        except AssertionError as e:
            logger.warning(e)
        except Warning as e:
            logger.warning(e) 
        except ValueError:
            logger.warning("-invalid points coordinates parameters.")
        except Exception as e:
            logger.debug(e)
            logger.error("-rectangle can not be created.")
        else:
            logger.info(self.canvas)

    def _bfs(self, x, y, sc, c):
        """
        breadth-first search for neighbor points connected to (x,y) with "colour" c
        """
        if (x, y) in self._canvas and self._canvas[y][x] == sc:
            self._canvas[y][x] = c

            self._bfs(x + 1, y, sc, c)
            self._bfs(x - 1, y, sc, c)
            self._bfs(x, y + 1, sc, c)
            self._bfs(x, y - 1, sc, c)
        else:
            
            return

    def create_bucket_fill(self, x, y, c=" "):
        """
        fill the entire area connected to (x,y) with "colour" c
        """
        try:
            if not self._canvas:
                raise Warning("-canvas was not created.")

            assert len(c) == 1 and isinstance(c, str), '-"color" parameter is not a symbol.'

            x, y = int(x) - 1, int(y) - 1
            if (x, y) in self._canvas:
                if c != self._canvas[y][x]:
                    self._bfs(x, y, self._canvas[y][x], c)
            else:
                raise AssertionError("-invalid point coordinates.")

        except AssertionError as e:
            logger.warning(e)
        except Warning as e:
            logger.warning(e) 
        except ValueError:
            logger.warning("-invalid point coordinates parameters.")
        except Exception as e:
            logger.debug(e)
            logger.error("-fill can not be created.")
        else:
            logger.info(self.canvas)
