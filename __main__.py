#!/usr/bin/env python3

from logging import getLogger
logger = getLogger("logger")


def main(argv):
    """
    startup
    """
    try:
        from argparser import parse_args
        from log import create_logger
        from parser import parse
        from paint import Paint

        args = parse_args(argv[1:])

        if args.command == "interactive":
            from sys import stdin, stdout
            input_stream = stdin
            output_stream = stdout
        else:
            input_stream = open(args.__dict__["in"], "r")
            output_stream = open(args.out, "w")

        create_logger(output_stream)

        pnt = Paint()

        commands = dict(
            C=pnt.create_canvas,
            L=pnt.create_line,
            R=pnt.create_rectangle,
            B=pnt.create_bucket_fill,
        )

        for arg in parse(input_stream):
            try:
                commands[arg[0]](*arg[1])
            except KeyError:
                logger.warning("-invalid command.")
            except TypeError:
                logger.warning("-invalid number of parameters.")
            except:
                raise

    except KeyboardInterrupt:
        
        return 0
    except FileNotFoundError:
        logger.warning("-file nod found.")

        return 1
    except PermissionError:
        logger.warning("-file permission denied.")

        return 1
    except Exception as e:
        logger.error(e)

        return 1
    else:

        return 0
    finally:
        try:
            if args.command != "interactive":
                input_stream.close()
                output_stream.close()
        except: pass


if __name__ ==  "__main__":
    from sys import exit, argv
    exit(main(argv))
