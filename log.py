def create_logger(stream):
    """
    create logger to given output stream
    """
    import logging
    import sys

    logger = logging.getLogger("logger")
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stream)
    logger.addHandler(handler)
