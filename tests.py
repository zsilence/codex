#!/usr/bin/env python3

import os, sys
import unittest
import tempfile

from os import linesep as ls

from paint import Paint

from log import create_logger


class PaintTestCaseMeta(type):
    """
    for tests generating
    """
    __testing_data = dict(
        create_canvas=(
            [
                [3, 3],
                ["one", 1],
                [1, -1],
                [5000, 1],
            ],
            [
                str().join(["-----", ls, ("|   |" + ls) * 3, "-----", ls]),
                "-invalid canvas size parameters." + ls,
                "-nonpositive canvas size parameters." + ls,
                "-canvas oversize." + ls,
            ],
        ),
        create_line=(
            [
                [1, 1, 1, 1, "xx"],
                [4, 1, -1, 1],
                [1, 1, 3, 3],
                ["q", 1, 1, 1],
                [3, 3, 3, 1]
            ],
            [
                '-"color" parameter is not a symbol.' + ls,
                "-invalid points coordinates." + ls,
                "-invalid points coordinates." + ls,
                "-invalid points coordinates parameters." + ls,
                str().join(["-----", ls, ("|  x|" + ls) * 3, "-----", ls]),
            ],
        ),
        create_rectangle=(
            [
                [1, 1, 1, 1, "xx"],
                [4, 3, 1, -1],
                ["q", 1, 2, 2],
                [3, 3, 1, 1],
                [2, 2, 2, 2, "o"],
            ],  
            [
                '-"color" parameter is not a symbol.' + ls,
                "-invalid points coordinates." + ls,
                "-invalid points coordinates parameters." + ls,
                str().join(["-----", ls, "|xxx|", ls, "|x x|", ls, "|xxx|", ls, "-----", ls]),
                str().join(["-----", ls, "|xxx|", ls, "|xox|", ls, "|xxx|", ls, "-----", ls]),
            ],
        ),
        create_bucket_fill=(
            [
                [1, 1, "ss"],
                [-1, 4, "c"],
                ["c"] * 3,
                [1, 3, "x"],
                [1, 1, "x"],
            ],
            [
                '-"color" parameter is not a symbol.' + ls,
                "-invalid point coordinates." + ls,
                "-invalid point coordinates parameters." + ls,
                str().join(["-----", ls, ("|xxx|" + ls) * 3, "-----", ls]),
                str().join(["-----", ls, ("|xxx|" + ls) * 3, "-----", ls]),
            ],
        ),
    )

    @staticmethod
    def __test_factory(name, params, outputs):
        @unittest.skipIf((params and outputs) is None, "test for {} method is not implemented yet".format(name))
        def test(self):
            cases = len(params)
            if cases != len(outputs): raise ValueError("len(params) != len(outputs)")

            for i in range(cases):
                with self.subTest(msg=outputs[i], i=i):
                    self.check_result(name, params[i], outputs[i])

        test.__name__ = "test_" + name

        return test

    @staticmethod
    def __test_generator():
        methods = (k for k in Paint.__dict__.keys() if k.startswith("create"))

        for method in methods:
            test = __class__.__test_factory(method, *__class__.__testing_data.get(method, (None, None)))

            yield test

    def __new__(cls, name, bases, attrs):
        for test in __class__.__test_generator():
            cnt = 0
            test_name = test.__name__
            while test_name in attrs:
                cnt += 1
                test_name = test.__name__ + hex(cnt)

            attrs[test_name] = test

        return super(__class__, cls).__new__(cls, name, bases, attrs)

class PaintTestCase(unittest.TestCase, metaclass=PaintTestCaseMeta):
    """
    for testing Paint class
    """
    @classmethod
    def setUpClass(cls):
        cls.temp_dir = tempfile.mkdtemp()
        os.chdir(cls.temp_dir)
        cls.out_stream = open("output.txt", "a+")
        create_logger(cls.out_stream)

    def setUp(self):
        self.pnt = Paint()
        self.pnt.create_canvas(3, 3)

    def check_result(self, name, params, output):
        self.out_stream.truncate(0)
        Paint.__dict__[name](self.pnt, *params)
        self.out_stream.seek(0, 0)
        self.assertEqual(output, self.out_stream.read())

    def test_create_bucket_fill_around(self):
        self.pnt.create_line(*[2] * 4)
        self.check_result("create_bucket_fill", [1, 1, "o"], str().join([
            "-----", ls, "|ooo|", ls, "|oxo|", ls, "|ooo|", ls, "-----", ls]))
        self.check_result("create_bucket_fill", [2, 2, "o"], str().join([
            "-----", ls, ("|ooo|" + ls) * 3, "-----", ls]))
        self.pnt.create_line(1, 2, 3, 2)
        self.check_result("create_bucket_fill", [1, 1], str().join([
            "-----", ls, "|   |", ls, "|xxx|", ls, "|ooo|", ls, "-----", ls]))

    def test_canvas_was_not_created(self):
        self.pnt = Paint()

        tests_params = dict(
            create_line=[1] * 4,
            create_rectangle=[1] * 4,
            create_bucket_fill=[1, 1, "o"],
        )

        for method_name, params in tests_params.items():
            with self.subTest(msg=method_name):
                self.check_result(method_name, params, "-canvas was not created." + ls)

    @classmethod
    def tearDownClass(cls):
        cls.out_stream.close()
        os.remove("output.txt")
        os.chdir("..")
        os.rmdir(cls.temp_dir)


if __name__ == "__main__":
    unittest.main(verbosity=2)
