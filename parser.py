def parse(stream):
    """
    paint commands generator from input stream lines
    """
    for line in stream:
        words = line.split()
        if not words: continue
        yield (words[0], words[1:])
