def parse_args(args):
    """
    for parsing args from command line
    """
    import argparse

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        title="subparsers",
        description="for interactive and file modes",
        dest="command",
        help="description"
    )

    file_parser = subparsers.add_parser("file", help="for specifying files names")
    file_parser.add_argument(
        "--in", "--input", "--infile",
        action="store", default="input.txt",
        help="specifying input file name"
    )
    file_parser.add_argument(
        "--out", "--output", "--outfile",
        action="store", default="output.txt",
        help="specifying output file name"
    )

    subparsers.add_parser("interactive", help="start interactive mode")

    if not args:
        args.append("file")

    return parser.parse_args(args)
